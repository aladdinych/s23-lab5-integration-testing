# Lab5 -- Integration testing

## Lab
1. Create your fork of the `
Lab5 - Integration testing
` repo and clone it. [***Here***](https://gitlab.com/aladdinych/s23-lab5-integration-testing)

2. Try to use InnoDrive application:

`script.google.com/macros/s/AKfycby1s4fIXUfPk8Oxue9RhFZ2_Tq2m-P2Bz5CapR3eo8KHd_c4qXwVyWfgKxwM-ZsnY8U/exec?service=getSpec&email=t.nugaev@innopolis.university`

            
    -       Here is InnoCar Specs:
            Budget car price per minute = 19
            Luxury car price per minute = 40
            Fixed price per km = 11
            Allowed deviations in % = 14.000000000000002
            Inno discount in % = 9

- To get the price for certain car ride, let's send next request:
`script.google.com/macros/s/AKfycby1s4fIXUfPk8Oxue9RhFZ2_Tq2m-P2Bz5CapR3eo8KHd_c4qXwVyWfgKxwM-ZsnY8U/exec?service=calculatePrice&email=t.nugaev@innopolis.university&type=_type_&plan=_plan_&distance=_distance_&planned_distance=_planned_distance_&time=_time_&planned_time=_planned_time_&inno_discount=_discount_`

## BVA Table

| Parameter        | Equivalence Class                |
| --------         |----------------------------------|
| type             | luxury; budget; nonsense; -      |
| plan             | minute; fixed_price; nonsense; - |
| distance         | <= 0; > 0; -                     |
| planned_distance | <= 0; > 0; -                     |
| time             | <= 0; > 0; -                     |
| planned_time     | <= 0; > 0; -                     |
| inno_discount    | yes; no; nonsense; -             |

where `-` is not specified.

## Decision Table

|     | `type` | `plan` | `distance` | `planned_distance` | `time` | `planned_time` | `inno_discount` | `Result` |
|-----| -------- | -------- | -------- | -------- | -------- | -------- | -------- | -------- |
| T1  | luxury | minute | > 0 | > 0 | > 0 | > 0 | yes | Valid |
| T2  | luxury | minute | > 0 | > 0 | > 0 | > 0 | no | Valid |
| T3  | luxury | minute | > 0 | > 0 | empty | > 0 | * | Invalid |
| T4  | luxury | minute | empty | empty | > 0 | > 0 | yes | Valid |
| T5  | luxury | minute | empty | empty | > 0 | > 0 | no | Valid |
| T6  | luxury | fixed_price | > 0 | > 0 | > 0 | > 0 | yes | Invalid |
| T7  | luxury | fixed_price | > 0 | > 0 | > 0 | > 0 | no | Invalid |
| T8  | budget | minute | > 0 | > 0 | > 0 | > 0 | yes | Valid |
| T9  | budget | minute | > 0 | > 0 | > 0 | > 0 | no | Valid |
| T10 | budget | minute | > 0 | > 0 | empty | empty | * | Invalid |
| T11 | budget | fixed_price | > 0 | > 0 | empty | empty | no | Invalid |
| T12 | budget | fixed_price | > 0 | > 0 | empty | empty | yes | Invalid |
| T13 | budget | fixed_price | empty | empty | > 0 | > 0 | * | Invalid |
| T14 | nonsense | * | * | * | * | * | * | Invalid |
| T15 | * | nonsense | * | * | * | * | * | Invalid |
| T16 | * | * | <= 0 | * | * | * | * | Invalid |
| T17 | * | * | * | <= 0 | * | * | * | Invalid |
| T18 | * | * | * | * | <= 0 | * | * | Invalid |
| T19 | * | * | * | * | * | <= 0 | * | Invalid |
| T30 | * | * | * | * | * | * | nonsense | Invalid |
| T21 | empty | * | * | * | * | * | * | Invalid |
| T22 | * | empty | * | * | * | * | * | Invalid |


## Test cases

I run the following test cases:

| # Test case | Link Params                                                                                           | Expected result | Actual result | Passed  |
| -------- |-------------------------------------------------------------------------------------------------------|-----------------| -------- |---------|
| T1          | &type=luxury&plan=minute&distance=1&planned_distance=1&time=30&planned_time=30&inno_discount=yes      | {"price":1092}  | {"price":1419.6000000000001} | Fail    |
| T2          | &type=luxury&plan=minute&distance=1&planned_distance=1&time=30&planned_time=30&inno_discount=no       | {"price":1200}  | {"price":1560} | Fail    |
| T3          | &type=luxury&plan=minute&distance=1&planned_distance=1&planned_time=30&inno_discount=yes              | Invalid Request | {"price":null} | Fail    |
| T4          | &type=luxury&plan=minute&time=30&planned_time=30&inno_discount=yes                                    | {"price":1092} | {"price":1560} | Fail    |
| T5          | &type=luxury&plan=minute&time=30&planned_time=30&inno_discount=no                                     | {"price":1200}  | {"price":1040} | Fail    |
| T6          | &type=luxury&plan=fixed_price&distance=1&planned_distance=1&time=30&planned_time=30&inno_discount=yes | Invalid Request | Invalid Request | Success |
| T7          | &type=luxury&plan=fixed_price&distance=1&planned_distance=1&time=30&planned_time=30&inno_discount=no  | Invalid Request | Invalid Request | Success |
| T8          | &type=budget&plan=minute&distance=1&planned_distance=1&time=5&planned_time=30&inno_discount=yes       | {"price":86.45} | {"price":86.45} | Success |
| T9          | &type=budget&plan=minute&distance=1&planned_distance=1&time=5&planned_time=30&inno_discount=no        | {"price":95}  | {"price":95} | Success |
| T10         | &type=budget&plan=minute&distance=1&planned_distance=1&planned_time=30&inno_discount=no               | Invalid Request | {"price":null} | Fail    |
| T11         | &type=budget&plan=fixed_price&distance=5&planned_distance=5&inno_discount=no                          | Invalid Request | {"price":null} | Fail    |
| T12         | &type=budget&plan=fixed_price&distance=5&planned_distance=5&inno_discount=yes                         | Invalid Request | {"price":null} | Fail    |
| T13         | &type=budget&plan=fixed_price&time=10&planned_time=10&inno_discount=yes                               | Invalid Request | {"price":151.66666666666666} | Fail    |
| T14         | &type=nonsense&plan=minute&distance=1&planned_distance=1&time=1&planned_time=1&inno_discount=yes      | Invalid Request | Invalid Request | Success |
| T15         | &type=budget&plan=nonsense&distance=1&planned_distance=1&time=1&planned_time=1&inno_discount=yes      | Invalid Request | Invalid Request | Success |
| T16         | &type=budget&plan=minute&distance=-1&planned_distance=1&time=1&planned_time=1&inno_discount=yes       | Invalid Request | Invalid Request | Success |
| T17         | &type=budget&plan=minute&distance=1&planned_distance=-1&time=1&planned_time=1&inno_discount=yes       | Invalid Request | Invalid Request | Success |
| T18         | &type=budget&plan=minute&distance=1&planned_distance=1&time=-1&planned_time=1&inno_discount=yes       | Invalid Request | Invalid Request | Success |
| T19         | &type=budget&plan=minute&distance=1&planned_distance=1&time=1&planned_time=-1&inno_discount=yes       | Invalid Request | Invalid Request | Success |
| T20          | &type=budget&plan=minute&distance=1&planned_distance=1&time=1&planned_time=1&inno_discount=nonsense   | Invalid Request | Invalid Request | Success |
| T21         | &plan=minute&distance=1&planned_distance=1&time=1&planned_time=1&inno_discount=yes                    | Invalid Request | Invalid Request | Success |
| T22         | &type=budget&distance=1&planned_distance=1&time=1&planned_time=1&inno_discount=yes                    | Invalid Request | Invalid Request | Success |



## Tests Results

* Incorrect calculations for luxury cars in minute plan.
* Incorrect calculations for luxury and budget cars if time is not present.
* Incorrect calculations for budget cars if distance is not present and fix_price is used.